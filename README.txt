3 pr�f�rences enregistr�es qui influences la page d'accueil :
- la luminosit�
- l'activation de la musique
- le volume de la musique

Ils sont modifiables dans les param�tres.
La validation des pr�f�rences s'effectue en appuyant sur le bouton retour
ou en quittant l'application.
Les pr�f�rences sont indiqu�es sur la page d'accueil.

Changer la langue du t�l�phone en anglais permet de changer la langue du texte
�crit sur les boutons.

Utilisation de la demande de permission pour l'utilisation des vibrations.

Le jeu se joue en local avec malheureusement un simpleTouch et ne se ferme pas
quand une partie est termin�e.