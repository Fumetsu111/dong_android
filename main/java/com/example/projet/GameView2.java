package com.example.projet;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

public class GameView2 extends SurfaceView implements SurfaceHolder.Callback{

    // déclaration de l'objet définissant la boucle principale de déplacement et de rendu
    private GameLoopThread2 gameLoopThread;
    private Balle2 balle; //définition de l'objet balle
    private Barre1 barre1; //définition de l'objet barre1
    private Barre2 barre2; //définition de l'objet barre2
    boolean gameOver = false; //indique si un joueur a marqué 5 point et donc a gagné

    private MediaPlayer soundPlayer1; //son indiquant que le joueur avec la barre1 a gagné
    private MediaPlayer soundPlayer2; //son indiquant que le joueur avec la barre2 a gagné

    private MainActivity3 m3; //définition de la page du jeu

    // création de la surface de dessin
    public GameView2(Context context) {
        super(context);
        getHolder().addCallback(this);
        gameLoopThread = new GameLoopThread2(this);

        // création d'un objet "balle",et les 2 "barre", dont on définira la largeur/hauteur
        // selon la largeur ou la hauteur de l'écran
        balle = new Balle2(this.getContext());
        barre1 = new Barre1(this.getContext());
        barre2 = new Barre2(this.getContext());

        //attribution des sons
        this.soundPlayer1 = MediaPlayer.create(context.getApplicationContext(), R.raw.player1);
        this.soundPlayer2 = MediaPlayer.create(context.getApplicationContext(), R.raw.player2);

        //création de la page du jeu
        this.m3 = new MainActivity3();
    }


    // Fonction qui "dessine" un écran de jeu
    public void doDraw(Canvas canvas) {
        if(canvas==null) {return;}

        // on efface l'écran, en blanc
        canvas.drawColor(Color.WHITE);

        // on dessine la balle et les barres
        balle.draw(canvas);
        barre1.draw(canvas);
        barre2.draw(canvas);
    }

    // Fonction appelée par la boucle principale (gameLoopThread)
    // On gère ici le déplacement des objets
    public void update() {
        //actualisation des coodonnées des barres, pour les communiquer à la balle
        balle.setInfoXB1(barre1.getX());
        balle.setInfoXB2(barre2.getX());
        balle.setInfoYB1(barre1.getY());
        balle.setInfoYB2(barre2.getY());

        balle.moveWithCollisionDetection(true);
    }

    // Fonction obligatoire de l'objet SurfaceView
    // Fonction appelée immédiatement après la création de l'objet SurfaceView
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        //actualisation des coodonnées des barres, pour les communiquer à la balle
        balle.setInfoXB1(barre1.getX());
        balle.setInfoXB2(barre2.getX());
        balle.setInfoYB1(barre1.getY());
        balle.setInfoYB2(barre2.getY());

        // création du processus GameLoopThread si cela n'est pas fait
        if(gameLoopThread.getState()==Thread.State.TERMINATED) {
            gameLoopThread=new GameLoopThread2(this);
        }
        gameLoopThread.setRunning(true);
        gameLoopThread.start();
    }

    // Fonction obligatoire de l'objet SurfaceView
    // Fonction appelée juste avant que l'objet ne soit détruit.
    // on tente ici de stopper le processus de gameLoopThread
    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        gameLoopThread.setRunning(false);
        while (retry) {
            try {
                gameLoopThread.join();
                retry = false;
            }
            catch (InterruptedException e) {}
        }
    }

    // Gère les touchés sur l'écran
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int currentX = (int)event.getX();
        int currentY = (int)event.getY();
        switch (event.getAction()) {

            // code exécuté lorsque le doigt touche l'écran.
            case MotionEvent.ACTION_DOWN:

                // si le doigt touche la balle, utilisation de cette partie du code pour les tests :
               // if(currentX >= balle.getX() &&
               //         currentX <= balle.getX()+balle.getBalleW() &&
               //         currentY >= balle.getY() && currentY <= balle.getY()+balle.getBalleH() ) {
               //     // on arrête de déplacer la balle
               //     balle.setMove(false);
               // }

                // si le doigt touche la barre1 :
                if(currentX >= barre1.getX() &&
                        currentX <= barre1.getX()+barre1.getBarreW()) {
                    // on arrête de déplacer la barre1
                    barre1.setMove(false);

                    //actualisation des coodonnées des barres, pour les communiquer à la balle
                    balle.setInfoXB1(barre1.getX());
                    balle.setInfoXB2(barre2.getX());
                    balle.setInfoYB1(barre1.getY());
                    balle.setInfoYB2(barre2.getY());

                }
                // si le doigt touche la barre2 :
                else if(currentX >= barre2.getX() &&
                        currentX <= barre2.getX()+barre2.getBarreW()) {
                    // on arrête de déplacer la barre2
                    barre2.setMove(false);

                    //actualisation des coodonnées des barres, pour les communiquer à la balle
                    balle.setInfoXB1(barre1.getX());
                    balle.setInfoXB2(barre2.getX());
                    balle.setInfoYB1(barre1.getY());
                    balle.setInfoYB2(barre2.getY());
                }
                break;

            // code exécuté lorsque le doight glisse sur l'écran.
            case MotionEvent.ACTION_MOVE:
                // on déplace la balle ou les barres sous le doigt du joueur
                // si elle est déjà sous son doigt (oui si on a setMove à false)

                //utilisation de cette partie du code pour les tests
                //if(!balle.isMoving()) {
                //    balle.setX(currentX);
                //    balle.setY(currentY);
                //}

                if(!barre1.isMoving()) {
                    barre1.setX(currentX);
                }
                else if(!barre2.isMoving()) {
                    barre2.setX(currentX);
                }

                //affichage du score si un point a été marqué
                if(balle.score){
                    Toast.makeText(getContext(), String.valueOf(balle.score1)+"     -     "+String.valueOf(balle.score2), Toast.LENGTH_LONG).show();
                    balle.score = false;
                }

                //affichage du score final si il y a un joueur à 5 points
                if(balle.score1 == 5 || balle.score2 == 5 && !gameOver){
                    Toast toast = Toast.makeText(getContext(), "Score final   :   "+String.valueOf(balle.score1)+"     -     "+String.valueOf(balle.score2), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP|Gravity.LEFT, 20, 900);
                    toast.show();
                    gameOver = true;

                    //lecture du son si le joueur avec la barre2 a gagné
                    if(balle.score1 == 5){
                        soundPlayer2.start();
                    }
                    //lecture du son si le joueur avec la barre1 a gagné
                    else{
                        soundPlayer1.start();
                    }
                }
                break;

            // lorsque le doigt quitte l'écran
            case MotionEvent.ACTION_UP:
                // on reprend le déplacement de la balle
                balle.setMove(true);
                barre1.setMove(true);
                barre2.setMove(true);

                //actualisation des coodonnées des barres, pour les communiquer à la balle
                balle.setInfoXB1(barre1.getX());
                balle.setInfoXB2(barre2.getX());
                balle.setInfoYB1(barre1.getY());
                balle.setInfoYB2(barre2.getY());
        }

        return true;  // On retourne "true" pour indiquer qu'on a géré l'évènement
    }

    // Fonction obligatoire de l'objet SurfaceView
    // Fonction appelée à la CREATION et MODIFICATION et ONRESUME de l'écran
    // nous obtenons ici la largeur/hauteur de l'écran en pixels
    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int w, int h) {
        //actualisation des coodonnées des barres, pour les communiquer à la balle
        balle.setInfoXB1(barre1.getX());
        balle.setInfoXB2(barre2.getX());
        balle.setInfoYB1(barre1.getY());
        balle.setInfoYB2(barre2.getY());

        balle.resize(w,h); // on définit la taille de la balle selon la taille de l'écran
        barre1.resize(w,h); // on définit la taille de la barre1 selon la taille de l'écran
        barre2.resize(w,h); // on définit la taille de la barre2 selon la taille de l'écran
    }
} // class GameView