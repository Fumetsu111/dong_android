package com.example.projet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

public class Settings2 extends AppCompatActivity {
    CheckBox music; //définition de la checkbox activation/désactivation de la musique
    SeekBar position; //définition de la barre de volume de la musique
    boolean musicB; //définition d'un boolean pour savoir le statut de la checkbox
    int positionB; //définition d'un int pour savoir la position du volume sonore
    SharedPreferences settings; //définition des préférences
    SeekBar lumi; //définition de la barre de luminosité
    int lumiB; //définition d'un int pour savoir la position du volume de luminosité

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //association des valeur à leur donner stocker grace à leur id
        music = findViewById(R.id.music);
        position = findViewById(R.id.position);
        lumi = findViewById(R.id.lumi);

        //création des préférences et récupération des données associées
        settings = getSharedPreferences("PREFS", MODE_PRIVATE);
        musicB = settings.getBoolean("MUSIC", true);
        positionB = settings.getInt("POSITION", 0);
        lumiB = settings.getInt("LUMINOSITE", 0);

        //repositionnement de la checkbox à l'état enregistre
        if(musicB){
            music.setChecked(true);
        }

        //repositionnement des seekbar aux l'endroits enregistrés
        position.setProgress(positionB);
        lumi.setProgress(lumiB);

        //activation de la musique si cocher des les préférences avec la checkbox
        music.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean b) {
                if (b) {
                    musicB=true;
                    saveSettingsBoolean("MUSIC", musicB);
                }
                else{
                    musicB=false;
                    saveSettingsBoolean("MUSIC", musicB);
                }
            }
        });

        //sauvegarde de l'emplacement de la seekbar du volume sonore définit par l'utilisateur
        position.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                saveSettingsInt("POSITION", i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //sauvegarde de l'emplacement de la seekbar du volume lumineux définit par l'utilisateur
        lumi.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                saveSettingsInt("LUMINOSITE", i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    //sauvegarde dans les préférences des valeus boolean
    public void saveSettingsBoolean(String s, boolean b){
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(s, b);
        editor.apply();
    }

    //sauvegarde dans les préférences des valeus int
    public void saveSettingsInt(String s, int i){
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(s, i);
        editor.apply();
    }

    public void pageHome(View view){
        startActivity(new Intent(this, MainActivity.class));
    }

}
