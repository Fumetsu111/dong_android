package com.example.projet;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity3 extends Activity {

    private GameView2 gameView; //définition de la vu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // On crée un objet "GameView" qui est le code principal du jeu
        gameView=new GameView2(this);
        // et on l'affiche.
        setContentView(gameView);
    }

} // class MainActivity3