package com.example.projet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class Barre2 {
    private BitmapDrawable img=null; // image de la barre
    private int x,y; // coordonnées x,y de la barre en pixel
    private int barreW, barreH; // largeur et hauteur de la barre en pixels
    private int wEcran,hEcran; // largeur et hauteur de l'écran en pixels
    private boolean move = true; // 'true' si la barre doit se déplacer automatiquement, 'false' sinon
    // pour déplacer la balle on ajoutera INCREMENT à ses coordonnées x et y
    private static final int INCREMENT = 5;
    private int speedX=INCREMENT, speedY=INCREMENT;

    // contexte de l'application Android
    // il servira à accéder aux ressources, dont l'image de la barre
    private final Context mContext;

    // Constructeur de l'objet "Barre2"
    public Barre2(final Context c)
    {
        x=450; y=0; // position de départ
        mContext=c; // sauvegarde du contexte
    }

    // on attribue à l'objet "Barre" l'image passée en paramètre
    // w et h sont sa largeur et hauteur définis en pixels
    public BitmapDrawable setImage(final Context c, final int ressource, final int w, final int h)
    {
        Drawable dr = c.getResources().getDrawable(ressource);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    // retourne 'true' si la barre se déplace automatiquement
    // 'false' sinon
    // car on la bloque sous le doigt du joueur lorsqu'il la déplace
    public boolean isMoving() {
        return move;
    }

    // définit si oui ou non la barre doit se déplacer automatiquement
    // car on la bloque sous le doigt du joueur lorsqu'il la déplace
    public void setMove(boolean move) {
        this.move = move;
    }

    // redimensionnement de l'image selon la largeur/hauteur de l'écran passés en paramètre
    public void resize(int wScreen, int hScreen) {
        // wScreen et hScreen sont la largeur et la hauteur de l'écran en pixel
        // on sauve ces informations en variable globale, car elles serviront
        // à détecter les collisions sur les bords de l'écran
        wEcran=wScreen;
        hEcran=hScreen;

        // on définit (au choix) la taille de la barre à 1/5ème de la largeur de l'écran
        barreW=wScreen/4;
        barreH=wScreen/4;
        img = setImage(mContext,R.mipmap.barre,barreW,barreH);
    }

    // définit la coordonnée X de la barre
    public void setX(int x) {
        this.x = x;
    }

    // définit la coordonnée Y de la barre
    public void setY(int y) {
        this.y = y;
    }

    // retourne la coordonnée X de la barre
    public int getX() {
        return x;
    }

    // retourne la coordonnée Y de la barre
    public int getY() {
        return y;
    }

    // retourne la largeur de la barre en pixel
    public int getBarreW() {
        return barreW;
    }

    // retourne la hauteur de la barre en pixel
    public int getBarreH() {
        return barreH;
    }

    // déplace la barre en détectant les collisions avec les bords de l'écran
    public void moveWithCollisionDetection()
    {
        // si on ne doit pas déplacer la barre (lorsqu'elle est sous le doigt du joueur)
        // on quitte
        if(!move) {return;}

        // on incrémente les coordonnées X
        x+=speedX;

        // si x dépasse la largeur de l'écran, on inverse le déplacement
        if(x+barreW > wEcran) {speedX=-INCREMENT;}

        // si y dépasse la hauteur l'écran, on inverse le déplacement
        if(y+barreH > hEcran) {speedY=-INCREMENT;}

        // si x passe à gauche de l'écran, on inverse le déplacement
        if(x<0) {speedX=INCREMENT;}

        // si y passe à dessus de l'écran, on inverse le déplacement
        if(y<0) {speedY=INCREMENT;}
    }

    // on dessine la barre, en x et y
    public void draw(Canvas canvas)
    {
        if(img==null) {return;}
        canvas.drawBitmap(img.getBitmap(), x, y, null);
    }

} // public class Barre2
