package com.example.projet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer; //définition de la musique
    Vibrator vibrator; //définition d'un objet Vivrator

    Button settings; //définition du bouton paramètre
    TextView music; //définition du texte indiquant l'état de la music "ON/OFF"
    TextView position; //définition d u texte indiquant le volume sonore
    TextView lumi; //définition d u texte indiquant le volume lumineux

    boolean musicB; //définition d'un boolean pour savoir le statut de la checkbox
    int positionB; //définition d'un int pour savoir la position du volume sonore
    int lumiB; //définition d'un int pour savoir la position du volume de luminosité

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //attribution de la musique
        this.mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.nier);

        //démarrage de la musique
        mediaPlayer.start();

        //création du vibrator
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        //association des valeur à leur donner stocker grace à leur id
        music = findViewById(R.id.music);
        position = findViewById(R.id.position);
        settings = findViewById(R.id.settings);
        lumi = findViewById(R.id.lumi);

        //changer de page, aller vers la page paramètre
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //arret de la musique au changement de page
                mediaPlayer.stop();
                startActivity(new Intent(MainActivity.this, Settings2.class));
            }
        });

        //création des préférences et récupération des données associées
        SharedPreferences settings = getSharedPreferences("PREFS",MODE_PRIVATE);
        musicB = settings.getBoolean("MUSIC", true);
        positionB = settings.getInt("POSITION", 0);
        lumiB = settings.getInt("LUMINOSITE", 0);

        //activation ou non de la musique selon les préférences
        if(musicB){
            music.setText("Music ON");
            if(mediaPlayer.isPlaying()){
                mediaPlayer.stop();
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
            }
            else{
                try {
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mediaPlayer.start();
            }
        }
        else{
            music.setText("Music OFF");
            mediaPlayer.stop();
        }

        //affichage du niveau de luminosité
        lumi.setText("Luminosite "+lumiB+"%");

        //affichage du volume sonore
        position.setText("Volume "+positionB+"%");

        //définition du volume selon les préférences
        mediaPlayer.setVolume((float)positionB/100, (float)positionB/100);

        //attribution du niveau de luminosité selon les préférences
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        float brightness=lumiB/(float)255;
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
    }

    //changer de page, aller vers la page de jeu
    public void pageStart(View view){
        mediaPlayer.stop();
        startActivity(new Intent(this, MainActivity3.class));
    }

    //faire vibrer
    public void vibrateFor500ms(View view) {

        if (vibrator.hasVibrator ()) {
            vibrator.vibrate (500); // pendant 500 ms
        }
    }

    //fermer l'application
    private void finish(View view) {
        finish(view);
    }
}
