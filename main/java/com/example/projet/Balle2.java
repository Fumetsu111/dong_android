package com.example.projet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;

class Balle2
{
    private MediaPlayer soundBarre; // définition du son quand la balle touche une barre
    private MediaPlayer soundPoint; //définition du son quand un joueur gagne un point
    private BitmapDrawable img=null; // image de la balle
    private int x,y; // coordonnées x,y de la balle en pixel
    private int balleW, balleH; // largeur et hauteur de la balle en pixels
    private int wEcran,hEcran; // largeur et hauteur de l'écran en pixels
    private boolean move = true; // 'true' si la balle doit se déplacer automatiquement, 'false' sinon

    boolean score = false; //vérification si l'affichage du score a été fais depuis le dernier point marqué
    int score1 = 0; //point joueur1
    int score2 = 0; //point joueur2

    private int infoYB1 = 1500; //coordonnée en y de la barre1
    private int infoXB1 = 450; //coordonée en x de la barre1
    private int infoYB2 = 0; //coordonnée en y de la barre2
    private int infoXB2 = 450; //coordonée en x de la barre2

    // pour déplacer la balle on ajoutera INCREMENT à ses coordonnées x et y
    private static float INCREMENT = 10;
    private float speedX=INCREMENT, speedY=INCREMENT;

    // contexte de l'application Android
    // il servira à accéder aux ressources, dont l'image de la balle
    private final Context mContext;

    // Constructeur de l'objet "Balle"
    public Balle2(final Context c)
    {
        //attribution des sons
        this.soundBarre = MediaPlayer.create(c.getApplicationContext(), R.raw.barre);
        this.soundPoint = MediaPlayer.create(c.getApplicationContext(), R.raw.point);

        x=450; y=900; // position de départ
        mContext=c; // sauvegarde du contexte
    }

    // on attribue à l'objet "Balle" l'image passée en paramètre
    // w et h sont sa largeur et hauteur définis en pixels
    public BitmapDrawable setImage(final Context c, final int ressource, final int w, final int h)
    {
        Drawable dr = c.getResources().getDrawable(ressource);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        return new BitmapDrawable(c.getResources(), Bitmap.createScaledBitmap(bitmap, w, h, true));
    }

    // retourne 'true' si la balle se déplace automatiquement
    // 'false' sinon
    // car on la bloque sous le doigt du joueur lorsqu'il la déplace
    public boolean isMoving() {
        return move;
    }

    // définit si oui ou non la balle doit se déplacer automatiquement
    // car on la bloque sous le doigt du joueur lorsqu'il la déplace
    public void setMove(boolean move) {
        this.move = move;
    }

    // redimensionnement de l'image selon la largeur/hauteur de l'écran passés en paramètre
    public void resize(int wScreen, int hScreen) {
        // wScreen et hScreen sont la largeur et la hauteur de l'écran en pixel
        // on sauve ces informations en variable globale, car elles serviront
        // à détecter les collisions sur les bords de l'écran
        wEcran=wScreen;
        hEcran=hScreen;

        // on définit (au choix) la taille de la balle à 1/5ème de la largeur de l'écran
        balleW=wScreen/15;
        balleH=wScreen/15;
        img = setImage(mContext,R.mipmap.ball,balleW,balleH);
    }

    // définit la coordonnée X de la balle
    public void setX(int x) {
        this.x = x;
    }

    // définit la coordonnée Y de la balle
    public void setY(int y) {
        this.y = y;
    }

    // retourne la coordonnée X de la balle
    public int getX() {
        return x;
    }

    // retourne la coordonnée Y de la balle
    public int getY() {
        return y;
    }

    // retourne la largeur de la balle en pixel
    public int getBalleW() {
        return balleW;
    }

    // retourne la hauteur de la balle en pixel
    public int getBalleH() {
        return balleH;
    }

    //définit la coordonnée y de la barre1
    public void setInfoYB1(int y){
        infoYB1 = y;
    }

    //définit la coordonnée y de la barre2
    public void setInfoYB2(int y){
        infoYB2 = y;
    }

    //définit la coordonnée x de la barre1
    public void setInfoXB1(int x){
        infoXB1 = x;
    }

    //définit la coordonnée x de la barre2
    public void setInfoXB2(int x){
        infoXB2 = x;
    }

    // déplace la balle en détectant les collisions avec les bords de l'écran ou les barres
    @SuppressLint("WrongConstant")
    public void moveWithCollisionDetection(Boolean mur)
    {
        if(mur){
            // si on ne doit pas déplacer la balle (lorsqu'elle est sous le doigt du joueur)
            // on quitte
            if(!move) {return;}

            // on incrémente les coordonnées X et Y
            x+=speedX;
            y+=speedY;

            // si x dépasse la largeur de l'écran, on inverse le déplacement
            if(x+balleW > wEcran) {speedX=-INCREMENT;}

            // si y dépasse la hauteur l'écran, on renvoi au milieu et on active le son de marcage d'un point
            if(y+balleH > hEcran) {x=450; y=900; score1++; score = true; soundPoint.start();}

            // si x passe à gauche de l'écran, on inverse le déplacement
            if(x<0) {speedX=INCREMENT;}

            // si y passe au dessus de l'écran, on renvoi au milieu et on active le son de marcage d'un point
            if(y<0) {x=450; y=900; score2++; score = true; soundPoint.start();}

            //si la balle entre en contact avec la barre1 on inverse le déplacement et on active le son de colision
            if(y-150 == infoYB1 && x > infoXB1-50 && x < infoXB1+260){speedY=-INCREMENT; soundBarre.start();}

            //si la balle entre en contact avec la barre2 on inverse le déplacement et on active le son de colision
            if(y-250 == infoYB2 && x > infoXB2-50 && x < infoXB2+260){speedY=INCREMENT; soundBarre.start();}
        }


    }

    // on dessine la balle, en x et y
    public void draw(Canvas canvas)
    {
        if(img==null) {return;}
        canvas.drawBitmap(img.getBitmap(), x, y, null);
    }

} // public class Balle2